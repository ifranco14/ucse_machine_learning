Trabajo Práctico 1: Análisis Exploratorio de Datos
Fecha y hora de entrega:

28/5/2018 18:00
Consigna:

A partir del dataset elegido el grupo debe realizar un análisis exploratorio en el cual se incluya:
1. Listado de variables y selección

    Por cada variable del dataset, explicar en una oración el contenido de dicha variable y definir si será utilizada como variable de entrada, de salida, o no será utilizada.
    Para la variable de salida especificar los valores posibles que puede tener.
    Para cada variable de entrada, especificar las transformaciones necesarias para poder utilizarla.
    Por cada variable que no se vaya a utilizar, explicar brevemente el motivo por el cual no será utilizada.
    
2. Análisis detallado de un conjunto de variables

    Para la variable de salida, explicar y graficar su balanceo y qué consecuencias va a tener eso a la hora de entrenar modelos.
    Para 5 variables de entrada (elegidas o no, pero incluyendo al menos 3 elegidas) graficar y explicar cómo se comportan y cómo afectan a la variable de salida (similar a lo que hicimos en clases con el dataset del Titanic).

Formato de entrega:

Cada grupo debe poseer un repositorio git (pueden utilizarse servicios como Github y Bitbucket) al cual los docentes deben tener acceso. En la raiz del mismo debe entregarse un Jupyter Notebook llamado tp1_exploration.ipynb.

El notebook debe poder ser ejecutado en su totalidad con python3, solamente clonando el repositorio y ejecutando Jupyter desde su raíz. Puede incluirse el dataset en el repositorio mismo, o puede incluirse código en el notebook que descargue el dataset al ser ejecutado.

Los gráficos deben mostrarse dentro del notebook mismo (no ser guardados como archivos aparte ni levantar ventanas de aplicaciones externas), utilizando cualquier biblioteca para gráficos que prefieran y permita eso. Las explicaciones y respuestas deben escribirse en celdas de tipo Markdown.

El repositorio debe incluir un archivo requirements.txt en la raíz donde se listen todas las dependencias necesarias para la ejecución del notebook (en el formato estándard de python).